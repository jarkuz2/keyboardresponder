//
//  ViewController.swift
//  KeyboardDemo
//
//  Created by Josh Campion on 06/04/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import UIKit

import KeyboardResponder
import TheDistanceCore

class ViewController: UIViewController {

    @IBOutlet weak var titleField:UITextField?
    
    @IBOutlet weak var nameField:UITextField?
    
    @IBOutlet weak var emailField:UITextField?
    
    @IBOutlet weak var textView:UITextView?
    
    @IBOutlet weak var scrollView:UIScrollView?
    
    var responder:KeyboardResponder!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        responder = KeyboardResponder()
        responder.scrollContainer = scrollView
        responder.inputAccessoryView = KeyboardResponderToolbar(navigationDelegate: responder)
        
        if let tf = titleField, tv = textView, ef = emailField, nf = nameField {
            responder.components = [.TextField(tf), .TextField(nf), .TextView(tv), .TextField(ef)]
        }
        
        textView?.textContainer.heightTracksTextView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func toggleDescription(sender: UISwitch) {
        textView?.hidden = !sender.on
        view.layoutSubviews()
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.responder.adjustScroll()
        })
    }
}

