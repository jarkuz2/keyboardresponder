//
//  KeyboardResponder.swift
//  KeyboardResponder
//
//  Created by Josh Campion on 26/01/2016.
//  Copyright © 2016 The Distance. All rights reserved.
//

import UIKit
import TheDistanceCore

/// Protocol that defines a view which contains a `KeyboardResponderInputType`. This can be useful when your input components are contained in other classes as and array of all of those contains can be created with a shared type - this protocol.
public protocol KeyboardResponderInputContainer {
    
    /// The component that can be navigated to be a `KeyboardResponder`.
    var inputComponent:KeyboardResponderInputType { get }
}

/// Enum that restricts the type of object that can become an `activeComponent` for a `KeyboardResponder`.
public enum KeyboardResponderInputType: Equatable {
    
    /// Input type is a `UITextField`.
    case textField(UITextField)
    
    /// Input type is a `UITextView`.
    case textView(UITextView)
    
    /// Convenience initialiser for creating a `KeyboardResponderInputType` from an arbitrary `UIView`. This fails if the `view` is not a `UITextField` or `UITextView`.
    public init?(view: UIView) {
        if let tv = view as? UITextView {
            self = .textView(tv)
        } else if let tf = view as? UITextField {
            self = .textField(tf)
        } else {
            return nil
        }
    }
    
    /// The `UIView` associated value for this enum type. This is used for applying methods on the shared properties of `UITextField` and `UITextView`.
    public var view:UIView {
        switch self {
        case .textField(let tf):
            return tf
        case .textView(let tv):
            return tv
        }
    }
}

/// Equatable conformance for `KeyboardResponderInputType`. Equality is defined if the associated value of both enums are equal.
public func ==(k1:KeyboardResponderInputType, k2:KeyboardResponderInputType) -> Bool {
    
    switch (k1, k2) {
    case (.textField(let tf1), .textField(let tf2)):
        return tf1 == tf2
    case (.textView(let tv1), .textView(let tv2)):
        return tv1 == tv2
    case (.textField(_), .textView(_)), (.textView(_), .textField(_)):
        return false
    }
}

/// This notification is observed by all `KeyboardResponder`s, and `adjustScroll()` is called. This recalcualtes the necessary offset to ensure the current `activeComponent` remains in view correctly. This can be called by objects containing text elements when those elements' position relative to the `scrollContainer` changes, but their `center` property does not. These situations are typically due to changes in layout affecting the text element's superview's siblings.
public let KeyboardResponderRequestUpdateScrollNotification = "KeyboardResponderRequestUpdateScroll"

/**

 `UITextField`s and `UITextView`s have their `delegate` properties assigned to this object when added to the `components` array. All delegate methods are forwarded to the `textFieldDelegate` and `textViewDelegate` objects respectively.
 
*/
open class KeyboardResponder: NSObject, KeyboardResponderInputAccessoryDelegate {

    // MARK: - Properties
    
    /// Array `KeyboardResponderInputType`s which define the navigation order.
    open var components = [KeyboardResponderInputType]() {
        didSet {
            activeComponent = nil
            
            for comp in components {
                
                // only assign an input accessory view if we have one set. Don't override an existing one with nil.
                if let iv = inputAccessoryView as? UIView {
                    (comp.view as? SettableInputAccessoryView)?.inputAccessoryView = iv
                }
                
                switch comp {
                case .textField(let tf):
                    tf.delegate = self
                case .textView(let tv):
                    tv.delegate = self
                }
            }
        }
    }
    
    /// Observes the center of the `activeComponent` to update the scroll offset as and when is necessary.
    fileprivate var centerObserver:ObjectObserver? = nil
    
    /// The currently active component. When a component dismisses this becomes `nil`. When a component becomes first responder, this value is automatically set.
    open var activeComponent:KeyboardResponderInputType? {
        willSet {
            // clear the previous obvserver to unregister with the object
            centerObserver = nil
        }
        didSet {
            if let newView = activeResponder {
                centerObserver = ObjectObserver(keypath: "center", object: newView, completion: { (_, _, change) -> () in
                    
                    guard let oldCenter = (change?[NSKeyValueChangeKey.oldKey] as? NSValue)?.cgPointValue,
                        let newCenter = (change?[NSKeyValueChangeKey.newKey] as? NSValue)?.cgPointValue else { return }
                    
                    if !oldCenter.equalTo(newCenter) {
                        self.adjustScroll()
                    }
                })
            }
        }
    }
    
    /// Convenience getter for the `UIResponder` subclass that is the current `activeComponent`.
    open var activeResponder:UIResponder? {
        return activeComponent?.view
    }
    
    /// Varaible that is assigned as the `inputAccessoryView` for each element in `components`.
    open var inputAccessoryView:KeyboardResponderInputAccessoryView? {
        didSet {
            configureKeyboardInputAccessoryView()
        }
    }
    
    /// Observes the will show notification to being scrolling.
    fileprivate var keyboardWillShowObserver:NotificationObserver?
    /// Observes the will hide notification to being scrolling.
    fileprivate var keyboardWillHideObserver:NotificationObserver?
    /// Observes update notification request as requested by an outside source.
    fileprivate var requestUpdateScrollObserver:NotificationObserver?
    
    /// The `UIScrollView` that contains the elements in `components` and has its scroll adjusted to ensure `activeComponent` is always visible.
    open var scrollContainer:UIScrollView? {
        didSet {
            
            if scrollContainer != nil {
                
              keyboardWillShowObserver = NotificationObserver(name: UIResponder.keyboardWillShowNotification.rawValue,
                    object: nil,
                    completion: keyboardWillAppear)
                
              keyboardWillHideObserver = NotificationObserver(name: UIResponder.keyboardWillHideNotification.rawValue,
                    object: nil,
                    completion: keyboardWillHide)
                
                requestUpdateScrollObserver = NotificationObserver(name: KeyboardResponderRequestUpdateScrollNotification,
                    object: nil,
                    completion: requestedUpdateScroll)
                
            } else {
                keyboardWillShowObserver = nil
                keyboardWillHideObserver = nil
                requestUpdateScrollObserver = nil
            }
        }
    }
    
    /// Property to temporarily prevent the automatic scrolling when the `activeComponent` changes. This can be useful when multiple changes are occuring at once and the `contentOffset` or `contentInset` is caluclated incorrectly. Simply set this to `true` change components and once layout has completed, set this to `true`, optionally calling `adjustScroll()`.
    open var preventAdjustScroll:Bool = false
    
    /// Used internally to adjust the `contentInset` to ensure the bottom elements are visible above the keyboard.
    fileprivate (set) open var bottomInsetBuffer:CGFloat?
    
    /// The padding between the top of the `inputAccessoryView` for the `activeComponent` and the onscreen keyboard. Default value is 16.0.
    open var keyboardActiveComponentBuffer:CGFloat = 16
    
    /// The current frame of the keyboard, used internally to maintain the correct scroll offset of inset.
    fileprivate (set) open var currentKeyboardRect:CGRect?
    
    /// Each `.TextField(let tf)` element in `components` has its `td.delegate` re-assigned to this `KeyboardResopnder` object. Each `UITextFieldDelegate` method is forwarded to this delegate object *after* the `KeyboardResponder` implementation. `UITextFieldDelegate` methods requiring a return value will return this delegate object's method's return value if it is implemented.
    open var textFieldDelegate:UITextFieldDelegate?
    
    /// Each `.TextView(let tv)` element in `components` has its `tv.delegate` re-assigned to this `KeyboardResopnder` object. Each `UITextViewDelegate` method is forwarded to this delegate object *after* the `KeyboardResponder` implementation. `UITextViewDelegate` methods requiring a return value will return this delegate object's method's return value if it is implemented.
    open var textViewDelegate:UITextViewDelegate?

    
    // MARK: - Keyboard Adjusting Methods
    
    /// Convenience method to dismiss the keyboard.
    open func dismissKeyboard() {
        scrollContainer?.endEditing(true)
    }
    
    /**
 
     Response to keyboard appearance. Updates the local variables and adjusts the scroll to the `activeComponent`.
     
     - parameter note: `NSNotification` for the `UIKeyboardWillShowNotification`.
     
    */
    open func keyboardWillAppear(_ note:Notification) {
        
      guard let kbFrame = (note.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue,
            let activeView = self.activeResponder, activeView.isFirstResponder && self.scrollContainer != nil else { return }
        
        self.currentKeyboardRect = kbFrame
        
        self.adjustScroll()
    }
    
    /// The function is called in response to any `KeyboardResponderRequestUpdateScrollNotification` notification. Subclasses can override this to modify the standard behaviour. The default behaviour is that update only occurs if
    open func requestedUpdateScroll(_ note:Notification) {
        if (note.object as? UIView) == self.activeComponent?.view {
            self.adjustScroll()
        }
    }
    
    func highestWindow() -> UIWindow? {
        
        var v = scrollContainer?.superview
        
        while v?.superview != nil {
            v = v?.superview
        }
        
        return v as? UIWindow
    }
    
    /// Takes the current keyboard size and adjusts `scrollContainer` bsed on this and `activeComponent`.
    open func adjustScroll() {
        
        guard let activeComponent = self.activeComponent,
            let scrollContainer = self.scrollContainer,
            let windowKeyboardRect = currentKeyboardRect,
            let kbFrame = highestWindow()?.convert(windowKeyboardRect, to: scrollContainer.superview), !preventAdjustScroll
            else { return }
        
        let deltaY = kbFrame.minY - scrollContainer.frame.origin.y
        
        guard deltaY > 0 else { return }
        
        let activeBottomY = activeComponent.view.frame.maxY + keyboardActiveComponentBuffer
        
        let activePointInScroll = scrollContainer.convert(CGPoint(x: 0, y: activeBottomY), from:activeComponent.view.superview)
        
        // Calculate the offset, ensuring the goal point doesn't pull down too far
        let goalPoint  = CGPoint(x: scrollContainer.contentOffset.x, y: max(activePointInScroll.y - deltaY, -scrollContainer.contentInset.top))
        
        // adjust the content inset to allow scrolling to the bottom of the view with the keyboard still up.
        var scrollFrame = scrollContainer.frame
        scrollFrame.size.height = min(scrollFrame.size.height, scrollContainer.contentSize.height)
        
        let kbIntersection = kbFrame.intersection(scrollFrame) // CGRectMake(0, 0, scrollContainer.contentSize.width, scrollContainer.frame.size.height))
        
        // ensure scroll can still happen even if the content is less than the view height but more that the size left on screen when there is a keyboard there
        let bottomDelta = kbIntersection.size.height + max(scrollContainer.frame.size.height - scrollContainer.contentSize.height, 0)
        
        if (scrollContainer.contentInset.bottom != bottomDelta) {
            self.bottomInsetBuffer = bottomDelta
            
            var insets = scrollContainer.contentInset
            insets.bottom = bottomDelta
            scrollContainer.contentInset = insets
        }
        
        scrollContainer.setContentOffset(goalPoint, animated: true)
    }
    
    /**
     
     Response to keyboard disappearance. Updates the local variables and resets the `contentInsets` after any adjustments for the `activeComponent`s.
     
     - parameter note: `NSNotification` for the `UIKeyboardWillHideNotification`.
     
     */
    open func keyboardWillHide(_ note:Notification) {
        
        guard let scrollContainer = self.scrollContainer else { return }
        
        self.activeComponent = nil
        
        var scrollInsets = scrollContainer.contentInset
        scrollInsets.bottom -= bottomInsetBuffer ?? 0
        scrollContainer.contentInset = scrollInsets
        bottomInsetBuffer = nil
        
        var insidePoint = scrollContainer.contentOffset
        insidePoint.y = max(min(insidePoint.y, scrollContainer.contentSize.height - scrollContainer.bounds.size.height), -scrollContainer.contentInset.top)
        scrollContainer.contentOffset = insidePoint
    }
    
    /// Convenience methods for refreshing the buttons on the `inputAccessoryView`.
    func configureKeyboardInputAccessoryView() {
        
        self.inputAccessoryView?.refreshBarButtonItemStatus()
    }
    
    //  MARK: - Navigation Delegate
    
    /**
     
     `KeyboardResponderInputAccessoryDelegate` implementation.
     
     - returns: `true` if the `activeComponent` is an element in `components` but not the last element.
     
    */
    open func inputAccessoryCanGoToNext(_ inputAccessoryView: KeyboardResponderInputAccessoryView) -> Bool {
        
        guard let active = activeComponent,
            let idx = components.index(of: active) else { return false }
        
        return idx >= 0 && idx < components.count - 1
    }
    
    /**
     
     `KeyboardResponderInputAccessoryDelegate` implementation.
     
     - returns: `true` if the `activeComponent` is an element in `components` but not the first element.
     
     */
    open func inputAccessoryCanGoToPrevious(_ inputAccessoryView: KeyboardResponderInputAccessoryView) -> Bool {
        
        guard let active = activeComponent,
            let idx = components.index(of: active) else { return false }
        
        return idx > 0
    }
    
    /**
     
     `KeyboardResponderInputAccessoryDelegate` implementation. Provided `activeComponent` is not the last element in `components`, `becomeFirstResponder()` is called on the element in `components` after the `activeComponent`.
     
     */
    open func inputAccessoryRequestsNext(_ inputAccessoryView: KeyboardResponderInputAccessoryView) {
        
        guard let active = activeComponent,
            let idx = components.index(of: active), idx < components.count - 1 else { return }
        
        components[idx + 1].view.becomeFirstResponder()
    }
    
    /**
     
     `KeyboardResponderInputAccessoryDelegate` implementation. Provided `activeComponent` is not the first element in `components`, `becomeFirstResponder()` is called on the element in `components` before the `activeComponent`.
     
     */
    open func inputAccessoryRequestsPrevious(_ inputAccessoryView: KeyboardResponderInputAccessoryView) {
        
        guard let active = activeComponent,
            let idx = components.index(of: active), idx > 0 else { return }
        
        components[idx - 1].view.becomeFirstResponder()
    }
    
    /**
     
     `KeyboardResponderInputAccessoryDelegate` implementation. Calls `dismissKeyboard()` to resign the current first responder.
     
     */
    open func inputAccessoryRequestsDone(_ inputAccessoryView: KeyboardResponderInputAccessoryView) {
        dismissKeyboard()
    }
}

// MARK: - UITextFieldDelegate

extension KeyboardResponder: UITextFieldDelegate {
    
    // MARK: UITextFieldDelegate Implementation
    
    /**
     
     Sets the active component to this element.
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
    */
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        // set the active component before UIKeyboardWillShowNotification is called
        self.activeComponent = .textField(textField);
        
        return textFieldDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    /**
     
     Configures `inputAccessoryView` and updates the scroll to the current `activeComponent`.
     
     */
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        configureKeyboardInputAccessoryView()
        adjustScroll()
        
        textFieldDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    /**
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
     */
    public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return textFieldDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    /**
     
     Forwards this message to the `textFieldDelegate`.
     
     */
    public func textFieldDidEndEditing(_ textField: UITextField) {
        textFieldDelegate?.textFieldDidEndEditing?(textField)
    }
    
    // MARK: Editing the Text Field’s Text
    
    /**
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
     */
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textFieldDelegate?.textField?(textField, shouldChangeCharactersIn: range, replacementString: string) ?? true
    }
    
    
    /**
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
     */
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return textFieldDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    /**
     
     Inputing a return character into a `UITextField` resigns the first responder status of the `UITextField`.
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
     */
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        
        return textFieldDelegate?.textFieldShouldEndEditing?(textField) ?? true
    }
}

// MARK: - UITextViewDelegate

extension KeyboardResponder: UITextViewDelegate {
    
    // MARK: UITextViewDelegate Implementation
    
    /**
     
     Sets the active component to this element.
     
     - returns: The response of the `textViewDelegate` if it is set, `true` otherwise.
     
     */
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.activeComponent = .textView(textView)
        
        return textViewDelegate?.textViewShouldBeginEditing?(textView) ?? true
    }
    
    /**
     
     Configures `inputAccessoryView` and updates the scroll to the current `activeComponent`.
          
     */
    public func textViewDidBeginEditing(_ textView: UITextView) {
        
        configureKeyboardInputAccessoryView()
        adjustScroll()
        
        textViewDelegate?.textViewDidBeginEditing?(textView)
    }
    
    /**
     
     - returns: The response of the `textFieldDelegate` if it is set, `true` otherwise.
     
     */
    public func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        return textViewDelegate?.textViewShouldEndEditing?(textView) ?? true
    }
    
    /**
     
     Forwards this message to the `textViewDelegate`.
     
     */
    public func textViewDidEndEditing(_ textView: UITextView) {
        
        textViewDelegate?.textViewDidEndEditing?(textView)
    }
    
    // MARK: Responding to Text Changes
    
    /**
     
     - returns: The response of the `textViewDelegate` if it is set, `true` otherwise.
     
     */
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return textViewDelegate?.textView?(textView, shouldChangeTextIn: range, replacementText: text) ?? true
    }
    
    /**
     
     Forwards this message to the `textViewDelegate`.
     
     */
    public func textViewDidChange(_ textView: UITextView) {
        
        textViewDelegate?.textViewDidChange?(textView)
    }
    
    // MARK: Responding to Selection Changes
    
    /**
     
     Forwards this message to the `textViewDelegate`.
     
     */
    public func textViewDidChangeSelection(_ textView: UITextView) {
        
        textViewDelegate?.textViewDidChange?(textView)
    }
    
    // MARK: Interacting with Text Data
    
    /**
     
     - returns: The response of the `textViewDelegate` if it is set, `true` otherwise.
     
     */
    public func textView(_ textView: UITextView, shouldInteractWith textAttachment: NSTextAttachment, in characterRange: NSRange) -> Bool {
        
        return textViewDelegate?.textView?(textView, shouldInteractWith: textAttachment, in: characterRange) ?? true
    }
    
    /**
     
     - returns: The response of the `textViewDelegate` if it is set, `true` otherwise.
     
     */
    public func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        return textViewDelegate?.textView?(textView, shouldInteractWith: URL, in: characterRange) ?? true
    }
}
