Pod::Spec.new do |s|
  s.name = 'KeyboardResponder'
  s.version = '0.1.5'
  s.license = { :type => 'MIT' }
  s.summary = 'Easy navigation between UITextFields and UITextViews.'
  s.homepage = 'https://github.com/thedistance'
  s.author       = { "The Distance" => "dev@thedistance.co.uk" }
  s.source = { :git => 'https://bitbucket.org/thedistance/keyboardresponder.git', :tag => s.version }
  s.swift_version = '4.2'

  s.dependency 'TheDistanceCore'

  s.ios.deployment_target = '8.0'

  s.source_files = 'KeyboardResponder/Classes/**/*.swift', 'KeyboardResponder/Extensions/**/*.swift', 'KeyboardResponder/Protocols/**/*.swift'
  s.requires_arc = true
end
